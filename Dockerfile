FROM ubuntu:22.04

# - bsdmainutils for 'hexdump'
# - perl needed by iverilog
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    && apt-get install --no-install-recommends -y \
                        ca-certificates \
                        cmake \
                        curl \
                        bsdmainutils \
                        g++ \
                        make \
                        perl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt

# Add OSS Cad Suite
RUN curl -sL https://github.com/YosysHQ/oss-cad-suite-build/releases/download/2022-05-20/oss-cad-suite-linux-x64-20220520.tgz | tar xz

# Add RISC-V toolchain
RUN curl -sL https://github.com/xpack-dev-tools/riscv-none-embed-gcc-xpack/releases/download/v10.2.0-1.2/xpack-riscv-none-embed-gcc-10.2.0-1.2-linux-x64.tar.gz | tar xz

ENV PATH "/opt/oss-cad-suite/bin:/opt/oss-cad-suite/py3bin:/opt/xpack-riscv-none-embed-gcc-10.2.0-1.2/bin:$PATH"

RUN cmake --version
RUN gcc --version
RUN iverilog -V
RUN riscv-none-embed-gcc --version
RUN verilator --version
RUN yosys --version

WORKDIR /work
